const express = require('express');
const axios = require('axios');
const app = express();

// Port for hsoting
const port = 3000;

// axios object that will be used for all requests to the officient server
const officient = axios.create({
  baseURL: "https://api.officient.io/1.0",
  timeout: 1000,
  headers:  { 'Authorization': 'Bearer ff539ad1f24b9ad8d18241156898b1a9858e3671'}
});


/**
 * Helper function
 * Used not to leak unwanted fields of objects in this api
 * Only listed fields will be kept (if they were present)
 */
const filtered_object = function(object,keys){
  return Object.keys(object).reduce(function(new_object, key) {
    if (keys.indexOf(key) !== -1) {
      new_object[key] = object[key];
    }
    return new_object;
  }, {});
};


/**
 * Allow our api to be used by everyone
 */
app.get('/*',function(req,res,next){
  res.header('Access-Control-Allow-Origin' , "*" );
  next();
});

// Api fields which are visible for a person
const PERSON_FIELDS = ["name", "id", "role_name"];

/**
 * get request returns list of people with the following information: "name", "id", "role_name"
 */
app.get('/people/', (req, res) => {
  officient.get('/people/list').then( response => {
    let people = response.data.data;
    let clean_people = [];
    for( let i in people){
      clean_people.push(filtered_object(people[i],PERSON_FIELDS));
    }
    res.send(clean_people);
  });
});

// Api fields which are visible for a detailed person
const DETAILED_PERSON_FIELDS = ["id","name", "address", "email", "weekly_time_engagement_minutes"];

/**
 * get request returns person details with the following information: "id","name", "address", "email", "weekly_time_engagement_minutes"
 */
app.get('/people/:personId', (req, res) =>{
  // combine a call to person details and to wage details to get all information
  let requests = [
    officient.get('/people/'+req.params.personId+'/detail'),
    officient.get('/wages/'+req.params.personId+'/current')
  ];
  Promise.all(requests)
  .then( responses => {
    let person = responses[0].data.data;
    person.weekly_time_engagement_minutes = responses[1].data.data.weekly_time_engagement_minutes;
    person.id = req.params.personId;

    let clean_person = filtered_object(person,DETAILED_PERSON_FIELDS);
    res.send(clean_person);
  });
});

app.listen(port);