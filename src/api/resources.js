/**
 * All api requests are handled here
 */

import Vue from 'vue'
import VueResource from 'vue-resource';

Vue.use(VueResource);


export default {
  /**
   * Load all employees from officient api
   */
  load_employees(){
    return new Promise((resolve) => {
      Vue.http.get("http://127.0.0.1:3000/people").then(response => {
        resolve(response.data)
      }, response => {
        // eslint-disable-next-line
        console.log(response)
      })
    });
  },

  /**
   * Request person details for a given person id using the officient api
   */
  load_employee(id){
    return new Promise((resolve) => {
      Vue.http.get("http://127.0.0.1:3000/people/"+id).then(response => {
        resolve(response.data)
      }, response => {
        // eslint-disable-next-line
        console.log(response)
      })
    });
  },


  /**
   * Get lambert coordinates for given address using geopunt api
   * @param address {line_1,Line_2,city}
   */
  address_to_lambert(address){
    let address_string = address.line_1 + " " + address.line_2 + " "+ address.city;
    return new Promise((resolve) => {
      Vue.http.get("https://loc.geopunt.be/geolocation/location?q="+encodeURIComponent(address_string)).then(response => {
        let location =response.data.LocationResult[0].Location;
        resolve({x:Math.round(location.X_Lambert72),y:Math.round(location.Y_Lambert72)})
      }, response => {
        // eslint-disable-next-line
        console.log(response)
      })
    });
  },

  /**
   * Calculate route using public transport form start to end
   * full api can be found at https://delijn.docs.apiary.io/#
   * @param start lambert coordinate
   * @param end lambert coordinate
   * @param moment moment.js object containing travel time
   * @param departure boolean departure or arrival time given
   * @returns {Promise<any>}
   */
  route(start,end,moment,departure){

    let path = "https://private-anon-eed58ec203-delijn.apiary-proxy.com/rise-api-core/reisadvies/routes/"

    path += departure?"Officient%20HQ/Home/":"Home/Officient%20HQ/";
    path += start.x + "/" + start.y + "/";
    path += end.x + "/" + end.y + "/";
    path += encodeURIComponent(moment.format("DD-MM-YYYY")) + "/" + encodeURIComponent(moment.format("HH:mm")) + "/";
    path += departure?1:2;
    path += "/on/on/on/on/of/nl";

    // De lijn api has frequent timeouts which are easily solved with a retry
    return new Promise((resolve) => {
      retry_get_timeout(5,path,resolve)
    });
  }

}

/**
 * recursive function, retries get request at most i times upon server timeout
 */
let retry_get_timeout = function(i,path,resolve){
  return Vue.http.get(path).then(response => {
      resolve(response.body.reiswegen[1]);
    }, response => {
      if( i>0){
        retry_get_timeout(i-1,path,resolve);
      }else {
        // eslint-disable-next-line
        console.log(response);
      }
    })

}