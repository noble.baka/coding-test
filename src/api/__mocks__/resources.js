/**
 * Mock the api calls for testing
 */

const api = jest.mock('@/api/resources.js');

function promise_mock(object){
  return jest.fn(() =>new Promise(resolve => resolve(object)))
}

api.load_employees = promise_mock([{"id":23333,"name":"Thomas Veldhuyzen","role_name":"CTO"}]);

api.load_employee = promise_mock({
  "id": "23333",
  "name": "Thomas Veldhuyzen",
  "email": "Thomas@veldhuyzen.be",
  "address": {
    "line_1": "Kastanjestraat 5",
    "line_2": "",
    "zipcode": "9840",
    "city": "De Pinte",
    "state": "",
    "country_code": "BE"
  },
  "weekly_time_engagement_minutes": {
    "monday": 96,
    "tuesday": 720,
    "wednesday": 420,
    "thursday": 300,
    "friday": 336,
    "saturday": 0,
    "sunday": 0
  }
});

api.address_to_lambert = promise_mock({ x: 103800, y: 199845 });

api.route = promise_mock({
  startTime: "00:00",
  endTime: "00:01",
  duration: "0u1min",
  reiswegstappen: [{
    startLocatie:"Home",
    aankomstLocatie: "Officient HQ",
    duration: 1,
    start: "00:00",
    end: "00:01",
    type:"WANDELEN"

  }]
});

module.exports = api;