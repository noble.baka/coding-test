import { shallowMount } from '@vue/test-utils'
import PersonDetail from '@/components/PersonDetail.vue'
import api from '@/api/resources.js'
import flushPromises from 'flush-promises'

jest.mock('@/api/resources.js');

describe('PersonDetail.vue', () => {

  it(' create_week', () => {
    const weekly_time_engagement_minutes = {
      "monday": 96,
      "tuesday": 720,
      "wednesday": 420,
      "thursday": 300,
      "friday": 336,
      "saturday": 0,
      "sunday": 0
    };

    const expected_week = [
      { morning:"Monday 09:00", evening:"Monday 10:36"},
      { morning:"Tuesday 09:00", evening:"Tuesday 21:00"},
      { morning:"Wednesday 09:00", evening:"Wednesday 16:00"},
      { morning:"Thursday 09:00", evening:"Thursday 14:00"},
      { morning:"Friday 09:00", evening:"Friday 14:36"}
      ];

    const week = PersonDetail.methods.create_week(weekly_time_engagement_minutes);

    expect(week).toHaveLength(5);
    for(let i in week){
      let day = week[i];
      expect(day.morning.format('dddd HH:mm')).toMatch(expected_week[i].morning);
      expect(day.evening.format('dddd HH:mm')).toMatch(expected_week[i].evening);
    }

  });

  it('travel_time on object load', async () => {
    const person = {"id":23333,"name":"Thomas Veldhuyzen","role_name":"CTO"};
    const pd = shallowMount(PersonDetail,{ propsData:{person:person}});
    await flushPromises();
    expect(api.route.mock.calls.length).toBe(10);
    expect(pd.vm.travel_time.asMinutes()).toBe(10);

  });
});