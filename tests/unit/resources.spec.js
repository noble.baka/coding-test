import api from '@/api/resources.js'
let moment = require('moment');

/**
 * Tests calls to external and internal api to see if the expected results are still contained
 */


describe('Api.js', () => {
  it('address_to_lambert', async () => {
    expect.assertions(1);

    const address= {line_1:"Kortrijksesteenweg 181",line_2:"",city:"9000 Gent"};
    const location = {x:104290,y:192363};
    const data = await api.address_to_lambert(address);
    expect(data).toEqual(location);
  });

  it('route', async () => {
    expect.assertions(10);

    const start = { x: 103800, y: 199845 };
    const end = {x:104290,y:192363};
    let m = moment();
    m.hour(9);
    m.minutes(0);
    m.seconds(0);
    m.day(7);
    const departure = false;
    const data = await api.route(start,end,m,departure);

    expect(data).toHaveProperty("startTime",expect.stringMatching(/\d\d:\d\d/));
    expect(data).toHaveProperty("endTime",expect.stringMatching(/\d\d:\d\d/));
    expect(data).toHaveProperty("duration",expect.stringMatching(/\d?\du\d\dmin/));
    expect(data).toHaveProperty("reiswegStappen", expect.any(Array));
    expect(data.reiswegStappen[0]).toHaveProperty("startLocatie","Home");
    expect(data.reiswegStappen.slice(-1)[0]).toHaveProperty("aankomstLocatie","Officient HQ");
    expect(data.reiswegStappen[0]).toHaveProperty("duration",expect.any(Number));
    expect(data.reiswegStappen[0]).toHaveProperty("start",expect.stringMatching(/\d\d:\d\d/));
    expect(data.reiswegStappen[0]).toHaveProperty("end",expect.stringMatching(/\d\d:\d\d/));
    expect(data.reiswegStappen[0]).toHaveProperty("type");

  });

  it('load employees', async () => {
    expect.assertions(1);
    const data = await api.load_employees();
    expect(data).toContainEqual({"id":23333,"name":"Thomas Veldhuyzen","role_name":"CTO"});
  });

  it('load employee', async () => {
    expect.assertions(1);
    const data = await api.load_employee(23333);
    expect(data).toEqual({
      "id": "23333",
      "name": "Thomas Veldhuyzen",
      "email": "Thomas@veldhuyzen.be",
      "address": {
        "line_1": "Kastanjestraat 5",
        "line_2": "",
        "zipcode": "9840",
        "city": "De Pinte",
        "state": "",
        "country_code": "BE"
      },
      "weekly_time_engagement_minutes": {
        "monday": 96,
        "tuesday": 720,
        "wednesday": 420,
        "thursday": 300,
        "friday": 336,
        "saturday": 0,
        "sunday": 0
      }
    });
  });
});